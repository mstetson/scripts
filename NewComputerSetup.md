# New Computer Setup

Do Initial out-of-box setup with micah@classroomsystems.com for iCloud ID and jennifer@stetsonnet.org for App Store ID.

## Install 1Password

- Install [1Password](https://1password.com/downloads/mac/) from their web site.
- Set up 1Password SSH Agent (use key names).
- Enable 1Password CLI.
- Enable git commit signing.



## Scriptable Stuff

I've scripted quite a bit of the setup and will continue to do more. Run this in a terminal:

```sh
curl https://bitbucket.org/mstetson/scripts/raw/master/setupNewMac | bash
```

Changes to default environment variables and synthetic root folders won't take effect until a reboot.



## AWS

Go to IAM and add a machine user for the new computer.
Then use `aws configure` to give this machine the access it needs.



## Register apps installed by scripts

Secrets are in 1Password.

- Log into Microsoft Office as micah@schoolsplp.com
- Log into Microsoft Teams as micah@schoolsplp.com
- Register Typora
- Register DataGraph



## Fonts

- [Proggy Vector](https://github.com/bluescan/proggyfonts)
- [Noto Sans](https://fonts.google.com/noto/specimen/Noto+Sans)
- Minion Pro – from [MyFonts](https://myfonts.com/), find the receipt and use the download link on it.



## To Install Outside the App Store

* [IJulia](https://github.com/JuliaLang/IJulia.jl): `using Pkg; Pkg.add("IJulia")`
* [ScreenFlow 9](http://dynamic.telestream.net/downloads/download-screenflow.asp?prodid=screenflow)
	Serial Number in 1Password
* [Acorn 6](https://flyingmeat.com/acorn/)
	Registration info in 1Password
* Basecamp Desktop App (on Basecamp menu inside the web app)
* [OmniOutliner](https://www.omnigroup.com/omnioutliner)
	Account info in 1Password
* [Zoom](https://zoom.us/download)
	Account info in 1Password
* [Descript](https://descript.com)


## From the App Store

* Affinity Designer 2
* Affinity Publisher 2
* Affinity Photo 2
* ColorSlurp
* Kindle
* Microsoft Remote Desktop
* OmniGraffle
* Skitch
* Soulver



## Packages I'm not installing until I need them

* [Alloy](https://alloytools.org/)
* [Spin](https://spinroot.com/)
* Mailto Interceptor Lite (from App Store)



## General

* Give the computer a name in System Preferences / Sharing. For 25 years, I've given my computers little-known names from the Bible.
* Add/remove a bunch of stuff from the dock.
* Install software updates.
* Make Skitch open at login
* Make Caffeine open at login
* Make ColorSlurp open at login (and show app icon in menu bar only)



## System Preferences

* Make caps-lock act as another command key. (Must be done for each keyboard, if there's more than one.)
* Speed up mouse tracking when using an external mouse.
* Disable gestures I tend to trigger inadvertently.
* Make desktop background a nice grey.
* Turn off auto-correct and automatic spell checking.
* Keyboard: Press fn key to show emoji and symbols.
* Turn off wallpaper tinting in windows.
* Turn on scroll bars at all times.
* Ensure Siri is completely disabled.
* Make Spotlight more about search and less about being "helpful".
* Show Bluetooth in the menu bar
* Accessibility &rarr; Display &rarr; Reduce transparency
* Add iCloud and other accounts.
* Enable location services for the following System Services
  - Find My Mac
  - Wi-Fi Networking



## Accounts for Apple mail, contacts, calendars

- Contacts: micah.stetson@gmail.com
- Calendars: micah@schoolsplp.com, micah.stetson@gmail.com
- Mail: Nothing for now, use Outlook.



## iCloud (now under Apple ID)

* Disable iCloud Drive
* Enable Reminders
* Enable Safari
* Enable Notes
* Disable Keychain
* Enable Find My Mac
* Disable everything else



## Finder Preferences

* General &rarr; New Finder windows show home folder
* Sidebar &rarr; Add home folder, remove iCloud Drive
* Advanced &rarr; Show all filename extensions
* Advanced &rarr; Remove items from the Trash after 30 days



## Safari Preferences

- Install 1Password Extension

- Adjust settings:

	* [ ] AutoFill &rarr; User names and passwords (1Password will do this instead)

	* [x] Advanced &rarr; Smart Search Field: Show full website address

	* [x] Advanced &rarr; Show Develop menu in menu bar

	* [x] View Menu &rarr; Show Status Bar

	* [ ] Edit Menu &rarr; Spelling and Grammar &rarr; Check Spelling While Typing

	* [ ] Edit Menu &rarr; Spelling and Grammar &rarr; Correct Spelling Automatically

	* [x] Develop Menu &rarr; Allow JavaScript from Apple Events



## Chrome Preferences

- Install 1Password Extension

- Turn off:

	* Appearance &rarr; Show warning before quitting with Cmd-Q

	* You and Google &rarr; Sync and Google services &rarr; Allow Chrome sign-in

	* You and Google &rarr; Sync and Google services &rarr; Autocomplete searches and URLs



## Outlook Preferences

- Set to green theme.
- Disable window transparency.
- Set to compact display size.
- Turn off event and message alerts.
- Add second time zone for calendar display: UTC+1 West Central Africa.
- Turn off spelling and grammar checks.
- Turn off AutoCorrect.
- Turn off AutoComplete and text predictions.



## Teams Preferences

- General
	- Auto-start application
- Notifications
	- Turn off missed-activity emails
	- Switch to Mac-style notifications
	- Notifications for mentions & replies, rather than all activity
- Devices: Jabra Evolve
	- Turn off noise suppression and automatic mic sensitivity.
- Files
	- Always open Word, PowerPoint, and Excel files in Desktop appS

## OmniGraffle Preferences

- Add my OmniGraffle template folder under Documents.

## GitUp Preferences

- Install command-line tool
- Advanced &rarr; Remotes: Automatically check: Never (otherwise, it pops up for fingerprint ID every 15 minutes)



## Typora Preferences

- Editor &rarr; Indent Size on Save: Tab
- Editor &rarr; NO auto-pair brackets and quotes
- Image &rarr; When Insert ... Copy image to current folder
- Image &rarr; Use relative path if possible
- Markdown &rarr; Indent Size for Code: 4



## Terminal Preferences

- Import Documents/Acme.terminal profile; set as default.



## Make $PWD work better

This should be done some other way, but for now, edit `/usr/local/plan9/rcmain`
to make the `cd` function used for interactive shells look like this:

```rc
		fn cd {
			# builtin cd $1 && flag i && awd
			# is not sufficient when running in a subshell
			# that is rc -e (like mk uses!)
			if(builtin cd $*){
				# Manage $PWD so symlinks work more nicely.
				if(~ $#PWD 0 || ~ $#* 0)
					PWD=`{pwd}
				if not
					PWD=`{$PLAN9/bin/cleanname -d $PWD $1}
				if(flag i) $PLAN9/bin/9 awd || status=''
				status=''
			}
		}
```



## Files

Use rsync to pull over all the documents, then filter them on the new machine

```bash
rsync -arlv micah@tertius.local:/Users/micah/Documents/. /Users/micah/Documents
```



## Checks the old computer for...

- Source code not committed to Bitbucket or Github.
- Scripts added to $HOME/bin without adding to this repo.
- Other apps and config I may have added without remembering to update this repo.

