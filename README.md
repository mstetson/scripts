Various little scripts
==============

Acme stuff
----------------

* `start-acme` – Start Acme with my preferred fonts and such, loading a dump file if there is one.
* `A+x` – Make the file shown in this Acme window executable.
* `Achanges` – Diff the contents of the window against what's currently saved on disk.
* `Agofmt` – Run the contents of the window through gofmt.
* `ind` – Run as |ind in the tag to add a level of indent to the selection.
* `unind` – Run as |unind in the tag to remove a level of indent from the selection.
* `noindent` – Pipe the selection through this to remove all indent.

Misc
-------

* `csv2tab` – Convert CSV to tab-delimited text. No escaping is performed.
* `g` – The g script from plan9port with the file patterns changed for my needs.
* `godocweb` – Start or restart the godoc web server on localhost:6060.
* `jsc` – Run Google's Closure compiler for JS with a bit more normal command-line syntax.
* `reload-matching` – Reload all pages in Safari whose URL contains the given string. E.g. `reload-matching localhost`.
* `tabledef` – Print a 'show create table' for the named MySQL table. E.g. `tabledef mysql.user`.
