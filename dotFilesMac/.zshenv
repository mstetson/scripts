export PLAN9=/usr/local/plan9
export EDITOR=editinacme
export PAGER=nobs

PATH=$HOME/bin
PATH=$PATH:$HOME/go/bin
PATH=$PATH:/opt/homebrew/bin
PATH=$PATH:/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin
PATH=$PATH:/usr/local/go/bin
PATH=$PATH:$PLAN9/bin
export PATH

export HOMEBREW_PREFIX=/opt/homebrew
export HOMEBREW_CELLAR=/opt/homebrew/Cellar
export HOMEBREW_REPOSITORY=/opt/homebrew
export MANPATH="/opt/homebrew/share/man${MANPATH+:$MANPATH}:"
export INFOPATH="/opt/homebrew/share/info:${INFOPATH:-}"

export NAMESPACE=/tmp/ns.micah.main

export KLONGPATH=/Users/micah/src/klong/lib


export GOPRIVATE=bitbucket.org/classroomsystems

export DLAP_JOBS_TOKEN='{{op://23pm62b425atbiifrpl5ceqr3q/crlajmnlkley6ruulxoojkevvm/credential}}'
