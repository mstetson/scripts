# MacOS overrides PATH (and maybe other stuff) set in .zshenv.
# Work around that by re-sourcing my .zshenv here.
. $HOME/.zshenv

test -d $NAMESPACE || mkdir $NAMESPACE

ulimit -n 8192
