#!/bin/bash

cat <<EOF
#################
Modifying sudo to use Touch ID.
You'll now be prompted for your password and then
to let Terminal administer your computer.
This script will modify the PAM configuration
so that instead of prompting for a password,
sudo will prompt for a fingerprint.

I love this.

EOF

cat <<EOF | sudo ed /etc/pam.d/sudo
1a
auth	sufficient	pam_tid.so
.
wq
EOF

