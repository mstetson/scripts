#!/bin/sh -e

echo Copying dotfiles into $HOME

cp -a dotFilesMac/. $HOME

chmod 0600 $HOME/.ssh/config
