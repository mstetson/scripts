#!/bin/sh

VERSION=1.20
PKG=go${VERSION}.darwin-arm64.pkg
URL=https://go.dev/dl/$PKG

echo Installing Go $VERSION from $URL

curl -L $URL > $HOME/Downloads/$PKG || exit 1
sudo installer -pkg $HOME/Downloads/$PKG -target / || exit 1

