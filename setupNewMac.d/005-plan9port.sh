#!/bin/bash -e -x

cd /usr/local
sudo mkdir plan9
sudo chown micah:admin plan9
chmod 775 plan9
git clone https://github.com/9fans/plan9port plan9
cd plan9
./INSTALL
